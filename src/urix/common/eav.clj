(ns urix.common.eav
  (:refer-clojure :exclude [= + - * /])
  (:require clojure.test))

(declare to_str one_var_clause constant_quote func_one_to_str funci_one_to_str get_var)
(defn query ;; покрыть тестами, отрефакторить и привести в божеский вид!!!!
  "Формирует SQL-запрос на основе EAV по сходному с Datalog принципу."
  [select table & clauses]
  (let [{vars :vars
         consts :consts
         funcsi :funcsi
         funcs :funcs} (reduce (fn [context prepared]
                                 (prepared (assoc context :table table)))
                               {:vars {} :consts [] :funcs [] :funcsi [] :cnt 0}
                               clauses)
        tables_all (concat (apply concat (vals vars)) consts)
        tables_uniq (reduce (fn [a [table n _]] (conj a [(to_str table) n]))
                            #{} tables_all)
        tables_names_uniq (reduce #(conj % (to_str (get %2 0))) #{} tables_all)
        uniq_name (loop [i 0] (if (pos?
                                   (count
                                    (filter #(re-seq (re-pattern (str "(?is)^t" i "\\d*")) %) tables_names_uniq)))
                                (recur (inc i))
                                (str "t" i)))
        tables_for_from_clause (clojure.string/join
                                ", "
                                (map (fn [[table n]] (str table " as " uniq_name n)) tables_uniq))
        constant_placeholder (fn [& _] "?")
        vars_clause (map #(one_var_clause % uniq_name) (vals vars))
        [consts_head consts_tail _] (reduce (fn [[h t i] [tb n f2 c]]
                                              [(conj h (str uniq_name n "." (to_str f2) "="
                                                            (constant_quote c)))
                                               (conj t c) (inc i)])
                                            [[] [] 0] consts)
        funcs_clause (concat
                      (map #(str "(" (funci_one_to_str vars uniq_name %) ")") funcsi)
                      (map #(str (to_str (first %)) "(" (apply func_one_to_str vars uniq_name (rest %)) ")") funcs))
        where_clause (clojure.string/join " AND " (flatten [vars_clause funcs_clause consts_head]))
        select_clause_map (into {} (map (fn [[var_name [[_ n f2]]]]
                                          [var_name (str (get_var vars uniq_name var_name) " as " var_name)])
                                        vars))
        select_clause (clojure.string/join
                       ", "
                       (map #(get select_clause_map % (str "'no var \"" % "\"' as " %)) select))]
    (str "SELECT " select_clause " FROM " tables_for_from_clause " WHERE " where_clause)))

(defn get_var [% uniq_name %2] (let [{[[_ n attr]] %2} %] (str uniq_name n "." (to_str attr))))

(defn process_one_func_el [% uniq_name %2]
  (cond
    (get % %2) (get_var % uniq_name %2)
    (and (clojure.test/function? %2) (get-in (%2 {}) [:funcsi 0]))
    (funci_one_to_str % uniq_name (get-in (%2 {}) [:funcsi 0]))
    (and (clojure.test/function? %2) (get-in (%2 {}) [:funcs 0]))
    (str (to_str (get-in (%2 {}) [:funcs 0 0])) "(" (func_one_to_str % uniq_name (get-in (%2 {}) [:funcs 0 1])) ")")
    (and (map? %2) (get %2 :placeholder)) "?"
    1 (constant_quote %2)))

(defn funci_one_to_str [vars uniq_name [head & [f_lst]]]
  (clojure.string/join
   " " (interpose (to_str head) (map #(process_one_func_el vars uniq_name %) f_lst))))
(defn func_one_to_str [vars uniq_name f_lst]
  (clojure.string/join
   ", " (map #(process_one_func_el vars uniq_name %) f_lst)))

(def constant_quote (fn [x] (if (re-seq #"^\d+$" (str x))
                              x
                              (str "'" (clojure.string/replace (to_str x) #"'" "''") "'")))) ;; '

(def to_str #(if (keyword? %) (name %) (str %)))

(defn one_var_clause [dimm prefix]
  (let [str_dimm (map (fn [[_ n f2]] (str prefix n "." (to_str f2))) dimm)]
    (mapcat (fn [a b] [(str a "=" b)]) str_dimm (rest str_dimm))))

(defn repack [{:keys [obj type vars consts table cnt] :as context}]
  (if (symbol? obj)
    (assoc context :vars (assoc vars obj (conj (get vars obj []) [table cnt type])))
    (assoc context :consts (conj consts [table cnt type obj]))))

(defn eav [e a v]
  (fn eav_prepared [{:keys [table cnt vars consts] :as context}]
    (assoc
     (reduce (fn [{vars :vars consts :consts} [obj type]]
               (repack (assoc context :obj obj :type type :vars vars :consts consts :table table :cnt cnt)))
             context
             [[e :entity] [a :attribute] [v :value]])
     :cnt (inc cnt))))

(defn teav [table e a v]
  (fn teav_prepared [{:keys [cnt vars consts] :as context}]
    (assoc
     (reduce (fn [{vars :vars consts :consts} [obj type]]
               (repack (assoc context :obj obj :type type :vars vars :consts consts :table table :cnt cnt)))
             (assoc context :cnt (inc cnt))
             [[e :entity] [a :attribute] [v :value]])
     :cnt (inc cnt))))

(defn fi "инфиксная функция" [func & args]
  (fn [{:as context :keys [funcs]}]
    (assoc context :funcsi (conj (get context :funcsi []) [func args]))))

(defn f "функция" [func & args]
  (fn [{:as context :keys [funcs]}]
    (assoc context :funcs (conj (get context :funcs []) [func args]))))

(defn ph "плейсхолдер" [& _]
  #_(fn [{:as context :keys [ph]}]
      (assoc context :ph (conj (get context :ph []) [ph])))
  {:placeholder 1})

(defn = [& args] (apply fi (cons '= args)))
(defn + [& args] (apply fi (cons '+ args)))
(defn - [& args] (apply fi (cons '- args)))
(defn * [& args] (apply fi (cons '* args)))
(defn / [& args] (apply fi (cons '/ args)))

(defn test-eav-query []
  (query '[a b c d] 'table
    (eav 'id :asd 10)
    (eav 'id :a 'a)
    (teav :t01 'id :q 'c)
    (teav "t00" 'id :q 'a)
    (eav 'id 'b "b's")
    (= 'id (+ 'c 111))
    (= (fi :#>> 'a "{\"a\", \"b\"}") "c")
    (f :xxx 10 (+ 'a (ph) (f :b) (f :qq 9 (- 'a 1)) 23) 20 'a 30)
    (eav 'id :asdx "x")))

(declare assert_result)
(defn test-eav []
  (assert (= (test-eav-query) assert_result) "Ошибк!"))

(def assert_result
  "SELECT t11.value as a, t14.attribute as b, t12.value as c, 'no var \"d\"' as d FROM t01 as t12, t00 as t13, table as t10, table as t11, table as t14, table as t15 WHERE t11.value=t13.value AND t10.entity=t11.entity AND t11.entity=t12.entity AND t12.entity=t13.entity AND t13.entity=t14.entity AND t14.entity=t15.entity AND (t10.entity = t12.value + 111) AND (t11.value #>> '{\"a\", \"b\"}' = 'c') AND xxx(10, t11.value + ? + b() + qq(9, t11.value - 1) + 23, 20, t11.value, 30) AND t10.attribute='asd' AND t10.value=10 AND t11.attribute='a' AND t12.attribute='q' AND t13.attribute='q' AND t14.value='b''s' AND t15.attribute='asdx' AND t15.value='x'")
