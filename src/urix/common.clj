(ns urix.common
  (:gen-class)
  (:require
   [clojure.core.match :refer [match]]
   [clojure.data.codec.base64 :as b64]
   [clj-time.core :as dt] [clj-time.format :as dtf] [clj-time.local :as dtl]))

(gen-class :name inter_process_locker_class
           :prefix "inter_process_locker_"
           :main nil
           :state state
           :init init
           :constructors {[clojure.lang.Keyword Object] []}
           :methods [[is_locked [] Object] [lock [] void] [unlock [] void]])
(defn inter_process_locker_init [type val] [[] (atom nil)])
(defn inter_process_locker_is_locked [this])
(defn inter_process_locker_lock [this])
(defn inter_process_locker_unlock [this])

(defn is_other_process_running [& {:keys [file port object]}]
  "http://rosettacode.org/wiki/Determine_if_only_one_instance_is_running"
  (if file
    (let [locker (-> file (java.io.RandomAccessFile. "rw") .getChannel .tryLock)]
      (if locker #(.release locker)))
    (if port (try (let [socket (java.net.ServerSocket. port 10 (java.net.InetAddress/getLocalHost))]
                    (if socket #(.close socket)))
                  (catch java.io.IOException e nil)))))

(defn convert_jdbc_to_clojure_java_jbdc [url & args]
  (let [[[_ driver path]] (re-seq #"^jdbc:([^:]+):(.+)" url)
        user_info (try (-> (str driver ":" path)
                           java.net.URI.
                           .getRawUserInfo)
                       (catch Exception e nil))
        [uri_user uri_pass] (if user_info (-> user_info (clojure.string/split #":")))
        [user pass] (or args [uri_user uri_pass])]
    (into
     (into
      {:subprotocol driver
       :subname (if user_info
                  (clojure.string/replace path (str user_info "@") "")
                  path)}
      (if user {:user user} {}))
     (if pass {:password pass} {}))))

(defn to_base64 [x]
  (let [baos (java.io.ByteArrayOutputStream.)]
    (.write baos (.getBytes x))
    (String. (b64/encode (.toByteArray baos)))))

(defn php-encode [d] ; http://php.net/manual/ru/function.serialize.php
  ; (-> "koi8-r" java.nio.charset.Charset/forName .newEncoder)
  ; [clj-php "0.0.7"] [com.sandinh/php-unserializer_2.10 "1.0.1"]
  (cond
    (map? d) (byte-array (concat (.getBytes (str "a:" (count d) ":{"))
                                 (reduce #(concat % (php-encode (get %2 0)) (php-encode (get %2 1))) [] (seq d)) 
                                 (.getBytes "}")))
    (or (seq? d) (vector? d)) (byte-array (concat (.getBytes (str "a:" (count d) ":{"))
                                                  (reduce #(concat % (php-encode (get %2 0)) (php-encode (get %2 1))) []
                                                          (map-indexed #(conj [%] %2) d)) 
                                                  (.getBytes "}")))
    (string? d) (.getBytes (str "s:" (count (.getBytes d)) ":\"" d "\";"))
    (integer? d) (.getBytes (str "i:" d ";"))
    (float? d) (.getBytes (str "d:" d ";"))
    (keyword? d) (php-encode (name d))
    (instance? (type (.getBytes "")) d)
    (byte-array (concat (.getBytes (str "s:" (count d) ":\"")) d (.getBytes "\";")))
    (nil? d) (.getBytes "N;")
    1 (.getBytes (str "s:" (count (str (.getBytes d))) ":\"" d "\";"))))

(defn php_parse [d start]
  (letfn [(error [x] (throw (Exception. x)))
          (pos [i] (get d (+ i start)))
          (get-int [d offs stop]
                   (cond
                     (get #{\- \0 \1 \2 \3 \4 \5 \6 \7 \8 \9} (get d offs)) (str
                                                                             (get d offs)
                                                                             (get-int d (+ 1 offs) stop))
                     (= (get d offs) stop) ""
                     1 (error "No int")))
          (get-float [d offs stop]
                     (cond
                       (get #{\- \. \0 \1 \2 \3 \4 \5 \6 \7 \8 \9} (get d offs)) (str
                                                                                  (get d offs)
                                                                                  (get-float d (+ 1 offs) stop))
                       (= (get d offs) stop) ""
                       1 (error "No float")))]
    (cond
      (and (= (pos 0) \N) (= (pos 1) \;)) [nil 2]
      (and (= (pos 0) \b) (= (pos 1) \:)) (let [n (get-int d (+ 2 start) \;)]
                                            [(if (= (Integer/parseInt n) 0) nil true) (+ 3 (count n))])
      (and (= (pos 0) \i) (= (pos 1) \:)) (let [n (get-int d (+ 2 start) \;)]
                                            [(Integer/parseInt n) (+ 3 (count n))])
      (and (= (pos 0) \d) (= (pos 1) \:)) (let [n (get-float d (+ 2 start) \;)]
                                            [(Double/parseDouble n) (+ 3 (count n))])
      (and (= (pos 0) \s) (= (pos 1) \:)) (let [_n (get-int d (+ 2 start) \:)
                                                n (Integer/parseInt _n)
                                                n_len (count _n)]
                                            (if (and
                                                 (= (pos (+ 5 n_len n)) \;)
                                                 (= (pos (+ 4 n_len n)) \")
                                                 (= (pos (+ 3 n_len)) \"))
                                              [(apply str (map #(pos (+ 4 n_len %)) (range n))) (+ 6 n_len n)]
                                              (error "String wrong format")))
      (and (= (pos 0) \a) (= (pos 1) \:)) (let [_n (get-int d (+ 2 start) \:)
                                                n (Integer/parseInt _n)
                                                n_len (count _n)
                                                pair (fn [d offs]
                                                       (let [[key_val key_len] (php_parse d offs)
                                                             [val_val val_len] (php_parse d (+ offs key_len))]
                                                         [{key_val val_val} (+ key_len val_len)]))
                                                pair_offs (+ start 4 n_len)
                                                [res_val res_len] (reduce
                                                                   (fn [[acc_val acc_len] _]
                                                                     (let [[pair_val pair_len] (pair d (+ acc_len pair_offs))]
                                                                       [(merge acc_val pair_val) (+ acc_len pair_len)]))
                                                                   [{} 0]
                                                                   (range n))]
                                            [res_val (+ 5 n_len res_len)])
      1 (error (str "Unknown token at " start)))))

(defn php_parsed_walk [d]
  (if (map? d)
    (into {} (map (fn [[k v]] [(php_parsed_walk k) (php_parsed_walk v)]) (seq d)))
    (get d 0)))

(defn php-decode [data]
  (php_parsed_walk (php_parse data 0)))

(defn obj-freeze [br]
  (let [baos (java.io.ByteArrayOutputStream.)
        out (java.io.ObjectOutputStream. baos)]
    (.writeObject out br)
    (.close out)
    (.toByteArray baos)))

(defn obj-thaw [blob]
  (let [in (java.io.ObjectInputStream. (java.io.ByteArrayInputStream. blob))
        out (.readObject in)]
    (.close in)
    out))

(defn spit-bytes [_fn data]
  (with-open [w (clojure.java.io/output-stream _fn)]
    (.write w data)))

#_(defn slurp-bytes [_fn] (clojure.contrib.io/to-byte-array (clojure.contrib.io/as-file _fn)))

#_(defn slurp-bytes [f]
  (let [file (clojure.java.io/file f)]
    (with-open [reader (clojure.java.io/input-stream file)]
      (let [buffer (byte-array (.length file))]
        (.read reader buffer)
        buffer))))

(defn slurp-bytes [is]
  "http://stackoverflow.com/questions/23018870/how-to-read-a-whole-binary-file-nippy-into-byte-array-in-clojure"
  (with-open [baos (java.io.ByteArrayOutputStream.)]
    (let [ba (byte-array 2000)]
      (loop [n (.read is ba 0 2000)]
        (when (> n 0)
          (.write baos ba 0 n)
          (recur (.read is ba 0 2000))))
      (.toByteArray baos))))

(defn fhtm [& x]
  (clojure.string/replace
   (apply str x)
   #"[<>&\n\t ]"
   #({"<" "&lt;" ">" "&gt;" "&" "&amp;" "\n" "<br/>\n" "\t" "&nbsp;&nbsp;&nbsp;&nbsp;" " " "&nbsp;"} %)))

(defn delete-recursively "https://gist.github.com/edw/5128978" [fname]
  ;; (-> ... clojure.java.io/file file-seq) (-> "/tmp" clojure.java.io/file .listFiles seq)
  (let [func (fn [func f]
               (when (.isDirectory f)
                 (doseq [f2 (.listFiles f)]
                   (func func f2)))
               (clojure.java.io/delete-file f))]
    (func func (clojure.java.io/file fname))))

(defn log-init [& {:keys [dir]}]
  (let [cnt (atom 0)]
    (try
      (delete-recursively dir)
      (catch Exception e))
    (clojure.java.io/make-parents (str dir "/.file"))
    (fn [type val & add]
      (let [line (fn [x] (str
                          "<b>"
                          (dtf/unparse (dtf/formatter-local "YYYY-MM-dd HH:mm:ss") (dtl/local-now))
                          "</b>: " x "<hr/>\n"))
            cline_toggle_f (str
                            "var chs=this.parentNode.parentNode.childNodes;"
                            "for(var i in chs){if(chs[i].style){"
                            "if(chs[i].style.display=='none'){"
                            "chs[i].style.display='block'}else{chs[i].style.display='none'}}};return 0;")
            cline (fn [s bord_color] (str
                                      "<div style=\"border:" bord_color " 1px solid\">\n"
                                      "  <div>\n"
                                      "    <b style=\"border:grey 1px solid\" onclick=\"" cline_toggle_f "\">+</b>\n"
                                      "  </div>\n"
                                      "  <div style=\"display:none\">\n"
                                      "    <b style=\"border:grey 1px solid\" onclick=\"" cline_toggle_f "\">-</b><br/>\n"
                                      s
                                      "  </div>\n"
                                      "</div>\n"))]
        (cond
          (= type :comment) (spit (str dir "/index.html") (line val) :append 1)
          (= type :error) (spit (str dir "/index.html") (line (str "<b style=\"color:red\">" val
                                                                   "</b>")) :append 1)
          (= type :request) (spit (str dir "/index.html") (cline (line (fhtm val)) "red") :append 1)
          (= type :response) (spit (str dir "/index.html") (cline (line (fhtm val)) "blue") :append 1)
          (= type :body) (spit (str dir "/index.html")
                               (line (str "<a href=\"data:text/html;base64," (to_base64 val)
                                          "\">content</a>")) :append 1)
          (= type :file)
          (let [ext (or (get (apply hash-map add) :ext) "txt")]
            (spit (str dir "/index.html") (line (str "<a href=\"" @cnt "." ext "\">" ext "</a>")) :append 1)
            (spit (str dir "/" @cnt "." ext) val)
            (swap! cnt inc)))))))

(defn http-log [_log & {:keys [all] :or {all nil}}]
  (let [get-content-type (fn [h]
                           (let [[[_ found]] (filter (fn [[k v]] (re-find #"(?is)^content-type$" k)) (seq h))]
                             found))
        _http-log (fn [tp rr log]
                    (match [tp rr]
                      [:response
                       {:request {:method meth :url url :proxy prx :headers req_h :body req_b}
                        :response {:code code :message mess :headers res_h :body res_b}}]
                      (when (or all (= "POST" meth)
                                (let [ct (get-content-type res_h)]
                                  (not (and ct (re-find #"javascript|css|image" ct)))))
                        (log :comment (str meth " " url))
                        (log :request (str "proxy: " prx "\n" meth " " url "\n"
                                           (reduce (fn [a [k v]] (str a k ": " v "\n")) "" (seq req_h))
                                           "\n" req_b))
                        (log :response (str code " " mess "\n"
                                            (reduce (fn [a [k v]] (str a k ": " v "\n")) "" (seq res_h))))
                        (log :body res_b))
                      [:request _] nil
                      [_ _] (log tp rr)))]
    (fn [t x] (_http-log t x _log))))

(defn current_html [page]
  #_(.asXml page)
  (str (.getJavaScriptResult
        (.executeJavaScript page
                            "document.getElementsByTagName(\"html\")[0].outerHTML"))))

(defn parse_request [request web_client]
  {:request
   {:method (str (.getHttpMethod request))
    :proxy [(-> web_client .getOptions .getProxyConfig .getProxyHost)
            (-> web_client .getOptions .getProxyConfig .getProxyPort)]
    :url (str (.getUrl request))
    :headers (.getAdditionalHeaders request)
    :body (.getRequestParameters request)
    :orig request}})
(defn parse_response [response]
  {:response
   {:code (.getStatusCode response)
    :message (.getStatusMessage response)
    :headers (reduce #(assoc %
                             (str (.getName %2))
                             (str (.getValue %2)))
                     {} (.getResponseHeaders response))
    :body (.getContentAsString response)
    :orig response}})

(defn web_client_request_processor [& {:keys [request log hook web_client connection try2]
                                       :or {hook (fn [& _]) log (fn [& _]) try2 nil}}]
  (let [req-resp (parse_request request web_client)]
    (log :request req-resp)
    (try
      (let [response (-> connection (.getResponse request))]
        (log :response (into req-resp (parse_response response)))
        (hook request response)
        response)
      (catch Exception e
        (log :response (into req-resp {:response {}}))
        (log :error
             (fhtm "Ошибка при получении ответа от сервера:\n\n" e "\n\n"
                   (.getMessage e) "\n\n" (reduce #(str % "\n" %2) (.getStackTrace e))))
        (if try2
          (do (log :comment "вторая попытка")
              (let [response (-> connection (.getResponse request))]
                (log :response (into req-resp (parse_response response)))
                (hook request response)
                response))
          (throw e))))))

(defmacro make_qw [dsn]
  `(let [~'c (apply convert_jdbc_to_clojure_java_jbdc ~dsn)]
     (fn [& args#]
       (let [[sql# & params#] args#]
         (cond
           (re-seq #"(?is)^\s*(?:select|desc)" sql#) (clojure.java.jdbc/query ~'c args#)
           (re-seq #"(?is)^\s*insert" sql#) (-> (clojure.java.jdbc/db-do-prepared-return-keys ~'c sql# (vec params#))
                                                seq vec (get-in [0 1]))
           1 (clojure.java.jdbc/execute! ~'c args#))))))

#_(defn make_qw [& dsn]
    (let [c (apply convert_jdbc_to_clojure_java_jbdc dsn)]
      (fn [& args]
        (let [[sql & params] args]
          (cond
            (re-seq #"(?is)^\s*(?:select|desc)" sql) (clojure.java.jdbc/query c args)
            (re-seq #"(?is)^\s*insert" sql) (-> (clojure.java.jdbc/db-do-prepared-return-keys c sql (vec params))
                                                seq vec (get-in [0 1]))
            1 (clojure.java.jdbc/execute! c args))))))

